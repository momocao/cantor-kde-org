---
layout: post
title: LabPlot integration with Cantor
date: 2016-07-23
---

LabPlot, software to data analysis and visualization, brings a new and cool feature in his 2.3.0 version: integration with Cantor.

That integration was implemented by Garvit Khatri during [GSoC 2015](http://garvitdelhi.blogspot.com/2015/08/final-evaluation.html). Now LabPlot users can perform calculations in some programming language supported by Cantor and do the plots, all it directly in LabPlot.

That is possible by the use of libcantorlibs, a set of widgets and libraries to bring the Cantor worksheet, panels and assistants to the software.
    
Currently LabPlot supports Maxima and Python backends, but more programming languages will come in next releases.
    
Thank you to Garvit Khatri and Alexander Semke for bring this feature to LabPlot, for use Cantor to do it, and for the fixes and improvements developed by you and implemented directly in Cantor.

Read more about this feature in [LabPlot 2.3.0 release announcement](http://labplot.sourceforge.net/2016/07/labplot-2-3-0-released/).
