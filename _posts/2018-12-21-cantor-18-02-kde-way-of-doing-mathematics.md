---
layout: post
title: Cantor 18.12 KDE Way of Doing Mathematics
date: 2018-12-21
---

*(Original post written by Alexander Semke in [Labplot's blog](https://labplot.kde.org/2018/12/21/cantor-18-12-kde-way-of-doing-mathematics/))*

Curious to read about Cantor on LabPlot’s homepage? This is easy to explain. Cantor has got quite a lot of development in the last couple of months, also with great contribution from LabPlot developers. There is a close collaboration between these two KDE projects which we hope to intensify even further in future and to make better use of the common code and human resources in order to provide a strong computational and visualization platform for scientific purposes.

In this blog post we want to highlight the more striking new features in Cantor 18.12 that was [recently released](https://www.kde.org/announcements/announce-applications-18.12.0.php). Since Cantor can run embedded in LabPlot (see the [LabPlot 2.3 release announcement](https://labplot.kde.org/2016/07/23/labplot-2-3-0-released/) for couple of examples), all the features described below are of course also available for users using Cantor from within LabPlot.

We invested quite a lot into improving the overall usability of Cantor’s worksheet. First improvement we want to mention is the **handling of long running and waiting commands**. In the past, when executing multiple commands at the same time, there was no feedback for the user which command is being calculated right now and which commands are waiting. In the current release we highlight the currently calculated command entry with a small animation of the prompt. The pending (meaning, queued but not being calculated yet) command entries are also highlighted so the user has the full picture of the processing status.

To bring more structure into the worksheet we allow the user to **format worksheet entries** by changing the background color, the foreground color and the font properties. The context menu of the command entry was extended to allow more faster access to these formatting options:

<img src="/assets/img/formatting_context_menu.png" class="img-fluid mx-auto d-block" />

These new formatting options can be used for example to highlight more important parts of the calculations or entries which the user wants to re-visit later again or to simply better format a longer worksheet document. On default, the background color of the command entries is set to the highlighting color of the desktop theme as shown on the following screenshot with an example Maxima session:

<img src="/assets/img/maxima_tutorial.png" class="img-fluid mx-auto d-block" />

New worksheet entries can be added via the context menu:

<img src="/assets/img/add_entries_context_menu.png" class="img-fluid mx-auto d-block" />

For faster creation of command entries there is furthermore a dedicated shortcut. To further improve the usability here we implemented the possibility to **place the cursor between two different entries**. Having this, a new command entry is automatically created and positioned between those two entries as soon as the user starts typing. The same happens when the user pastes some text at the current position of the cursor.

Some computer algebra systems like Maxima provide the ability to get the **documentation for commands** directly from the interactive prompt (see also [Maxima Manual](http://maxima.sourceforge.net/docs/manual/maxima_3.html)). For multiple choices, Maxima needs to ask back for further input. We improved this workflow and made it more stable. Furthermore, to better discriminate this communication with Maxima asking for additional input from the usual input in the command entries we changed the color and the font for these entries in the worksheet. Also, the status of the command entry is set to “Calculating” and the blinking prompt makes clear some further input is required.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YDQ1WwO09kc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="mx-auto d-block" allowfullscreen></iframe>

When using Cantor for calculations and the documentation of them, the user has the choice between different entry types that serve different needs. Cantor provides the worksheet entry types “command”, “Latex”, “text” and “image”. In the 18.12 release this list was extended. With the added **support for Markdown**, the user can now use this markup language to format the text in the worksheet.

When sharing the project files there can be situations where the actual computer algebra system or the programming language used in these project files are not available on the target system. In the past it was not possible to open Cantor’s worksheets in case the “backend system” is not available. In Cantor 18.12 we refactored the code and the way how and when we communicate with the backend. In cases when the backend is not available, the worksheet is opened in **read-only mode** and all the editing capabilities, also the the actual execution of calculations, are disabled. With this we still allow the user to consume the document and the editing/calculation features are enabled once the actual backend is installed on the target system.

Finally, Cantor has got his own entry on [KDE Store](https://store.kde.org/) – the central place for the content in and around the KDE desktop and applications. Now the user can download Cantor project files published by other users from within the application:

<img src="/assets/img/cantor_add_ons.png" class="img-fluid mx-auto d-block" />

At the moment there is not much [content](https://www.opendesktop.org/browse/cat/496/) but we hope to get more projects published by users in future.

A lot of bug fixing was done for 18.12. Many smaller and bigger features and improvements were implemented and not of all them were described in this post. We refer to the [Change Log](https://phabricator.kde.org/source/cantor/browse/master/ChangeLog) for the more or less complete list of changes.

There is still a lot of work to be done and we have already a lot of ideas and items in the development backlog. If you are interested in contribution to this project, don’t hesitate to approach us. Testing, coding, writing and publishing of examples – any kind of help is welcome!
