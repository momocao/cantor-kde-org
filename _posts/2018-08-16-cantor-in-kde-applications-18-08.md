---
layout: post
title: Cantor in KDE Applications 18.08
date: 2018-08-16
---

Cantor new version was released with [KDE Applications 18.08](https://www.kde.org/announcements/announce-applications-18.08.0.php) bundle. The main changes are:

* Support to Python 3.7;
* Recommended Sage version changed to 8.1 and 8.2;
* Improve LaTeX worksheet export;
* A lot of bug fixes.

Read the complete changelog for this version of Cantor in [KDE Applications 18.08 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0).
