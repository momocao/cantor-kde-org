---
layout: post
title: Cantor in KDE Applications 18.04
date: 2018-04-19
---

Cantor new version was released with [KDE Applications 18.04](https://www.kde.org/announcements/announce-applications-18.04.0.php) bundle. The main changes are:

* Add shortcuts to evaluate and interrupt the running calculation;
* Port Qalculate and Scilab to QProcess;
* A lot of bug fixes;
* R backend is now fixed!

Read the complete changelog for this version of Cantor in [KDE Applications 18.04 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0).
