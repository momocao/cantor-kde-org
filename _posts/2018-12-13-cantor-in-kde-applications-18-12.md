---
layout: post
title: Cantor in KDE Applications 18.12
date: 2018-12-13
---

Cantor new version was released with [KDE Applications 18.12](https://kde.org/announcements/announce-applications-18.12.0) bundle. The main changes are:

* Add Markdown entry type;
* Animated highlighting of the currently calculated command entry;
* Visualization of pending command entries (queued, but not being calculated yet);
* Allow to format command entries (background color, foreground color, font properties);
* Allow to insert new command entries at arbitrary places in the worksheet by placing the cursor at the desired position and by start typing;
* For expressions having multiple commands, show the results as independent result objects in the worksheet
* Add support for opening worksheets by relative paths from console;
* Add support for opening multiple files in one Cantor shell;
* Change the color and the font for when asking for additional information in order to better discriminate from the usual input in the command entry;
* Added shortcuts for the navigation across the worksheets (Ctrl+PageUp, Ctrl+PageDown);
* Add action in 'View' submenu for zoom reset;
* Enable downloading of Cantor projects from store.kde.org (at the moment upload works only from the website);
* Open the worksheet in read-only mode if the backend is not available on the system.

Read the complete changelog for this version of Cantor in [KDE Applications 18.12 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0).
